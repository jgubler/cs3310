#!/usr/bin/python
#James Gubler
#RSA Encryption
#Oct. 18 2013

import math
import random

ALPHABET26 = "abcdefghijklmnopqrstuvwxyz"
ALPHABET70 = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789,.?! \t\n\r"

BIGKEY1 = "lucasipsumdolorsitamethanendormoffahsokafettkitjinnchewbaccaskywalkermarawedgehothchewbaccaluukeskywalkerbabaleiamoncadewicketackbarsolojadedarthorganasithleiasolocpomoffamidaladookumonsidiousdantooinemoffbobababadagobahfetttatooineowensolomandaloresithcalrissianbenbespingrievousdagobahcadedarthskywalkerahsokadroidtatooinedarthjardarthjabbajangomacedarthcoruscantdarthwicketwampadarthamidalamoffjabbawindudantooineamidalaanakin"
BIGKEY2 = "bespinskywalkerdagobahjabbajangoobiwandarthhuttyodawickethuttmoffskywalkerorganakesselkenobiorganagrievousjangohuttdookuvadermoffyodathrawnmaulskywalkermoffpondakashyyykyodaorganadarthmaradookubinksfistordlandobespinskywalkerackbardarthahsokaorganaskywalkermonmoffjabbahothleiamoffbinksmacesolofistotatooineamidalatatooinecpcadewindufistomoffcadeskywalkerdagobahfettamidalaskywalkerkitthrawnmandalorepalpatineluuke" 
#----------------string-value/value-string----
    # value = 70*x + r
def valueToString(value, alphabet):
    power70 = []
    newString = ""
    
    while value >= len(alphabet):
        x = value / len(alphabet)
        remainder = value % len(alphabet)
        power70.append(remainder)
        value = x
    power70.append(value)
    power70.reverse()
    for num in power70:
        
        newString += alphabet[num]
    return newString
    
    
def stringToValue(s,alphabet):
    value = 0
    for letter in s:
        value = (value * (len(alphabet)) + alphabet.find(letter))
    return value

def stringToValueKeys(s,alphabet):
    value = 0
    for letter in s:
        value = (value * (len(alphabet)) + alphabet.find(letter)) % (10**200) 
    return value
#------------------class RSA----------------
class RSA:
    def __init__(self):
        print "initiated RSA"
#----------Encryption-----------------------
    def Encrypt(self,filein,fileout):
        fin = open(filein, "rb")
        plainText = fin.read()
        
        
        public = open("public.txt", "rb")
        n = public.readline()
        e = public.readline()
#        n = 2537
#        e = 13
        public.close()
        maxLettersPerBlock = 108
#        maxLettersPerBlock = 108
        listOfBlocks = []
        block = ""
        for char in plainText:
            block += char
            if (len(block)+1) > maxLettersPerBlock:
                listOfBlocks.append(block)
                
                block = ""
        listOfBlocks.append(block)
        print block
        blockNums = []
        for elt in listOfBlocks:
            blockNums.append(stringToValue(elt,ALPHABET70))
        encryptedNums = []
        decryptedNums = []
#        private = open("private.txt", "rb")
#        n = private.readline()
#        d = private.readline()
#        n = 2537
#        d = 937
#        private.close()

        for num in blockNums:
            print num
            c = pow(num,int(e),int(n))
            print c
            encryptedNums.append(c)
#something wrong right here
#            s = valueToString(c,ALPHABET70)
#            v = stringToValue(s,ALPHABET70)
#            m = pow(c,int(d),int(n))
#            decryptedNums.append(m)
#            print m
#            print m == num
            
        encryptedString = ""
        #decryptedString = ""
       
        for eNum in encryptedNums:
            encryptedString += valueToString(eNum,ALPHABET70)+"$"
#        for dNum in decryptedNums:
#            decryptedString += valueToString(dNum,ALPHABET70)
        fout = open(fileout, "wb")
#        print "-------------------------------------------"
#        print encryptedString
#        print "-------------------------------------------"
        fout.write(encryptedString)
        fin.close()
        fout.close()  
        print "encrypted"
#----------Decryption-----------------------
    def Decrypt(self,filein,fileout):
        fin = open(filein,"rb")
        encryptedString = fin.read()
        
        print encryptedString
        print "---------------------------------------"
        fin.close()
        private = open("private.txt", "rb")
        n = private.readline()
        d = private.readline()
#        n = 2537
#        d = 937
        private.close()

        listOfBlocks = []
        block = ""
        for char in encryptedString:
            if char == "$":
                listOfBlocks.append(block)
                print block
                print "-----------------------------------"
                block = ""
            else:
                block += char
        blockNums = []
        for elt in listOfBlocks:
            blockNums.append(stringToValue(elt,ALPHABET70))
        decryptedNums = []
        for num in blockNums:
            m = pow(num,int(d),int(n))
            decryptedNums.append(m)
        decryptedString = ""
        for dNum in decryptedNums:
            decryptedString += valueToString(dNum,ALPHABET70)
        fout = open(fileout,"wb")
        fout.write(decryptedString)
        fout.close()
        print "decrypted"
#---------------Key-gen----------------------
    def GenerateKeys(self,keystring1,keystring2):
        key1 = stringToValueKeys(keystring1,ALPHABET26)
        key2 = stringToValueKeys(keystring2,ALPHABET26)
        if (key1 % 2) != 1:
            key1 += 1
        if (key2 % 2) != 1:
            key2 += 1
        notPrime1 = True
        while notPrime1:
            if isPrime(key1):
                notPrime1 = False
            else:
                key1 += 2
        notPrime2 = True
        while notPrime2:
            if isPrime(key2):
                notPrime2 = False
            else:
                key2 += 2
        # n = p*q
        n = key1*key2
        # r = (p-1)*(q-1)
        r = (key1-1)*(key2-1)
        # e - 100 dig prime num that doesn't evenly divide into
        e = (10**398) + 1
        divisible = True
        while divisible:
            notPrime3 = True
            while notPrime3:
                if isPrime(e):
                    notPrime3 = False
                else:
                    e += 2
            if (r % e) != 0:
                divisible = False
            else:
                e += 2
        # d inverse of e mod r //// (d*e)%r=1
        d = findInverse(e,r)
        # save e and n to a public text file
        fileout = open("public.txt","wb")
        fileout.write(str(n)+"\n"+str(e)+"\n")
        fileout.close()
        # save n and d to a private text file
        privout = open("private.txt","wb")
        privout.write(str(n)+"\n"+str(d)+"\n")
        privout.close()
        #everything else i don't care about
         
#---------------extended-euclidean------------------
def extendedGCD(a,b):
    return extendedGCDR(a,b) 

def extendedGCDR(a,b):
    if a == 0:
        return (b,0,1)
    else:
        g,y,x = extendedGCDR(b%a,a)
        return (g,x-(b//a)*y,y)

def findInverse(a,r):
    g,x,y = extendedGCD(a,r)
    if g != 1:
        return None
    else:
        return x % r
#---------------Prime-Tester-----------------
def isPrime(num):
    if num == 1 or num == 2:
        return True
    kIter = 10
    for ki in range(kIter):
        randomB = random.randrange(2,num)
        passes = millerTest(num,randomB)
        if not passes:
        	return False
    return True

def millerTest(num, randomB):
    #(2**expcount)*T=num-1
    if num % 2 != 1:
        return False
    newnum = num-1
    expcount = 0
    newexp = False
    while not newexp:
        T = newnum / 2
        expcount += 1
        if T % 2 != 0:
            newexp = True
        else:
            newnum = T
    case1 = pow(randomB,T,num)
    if case1 == 1:
        return True
    for J in range(expcount):
        case2 = pow(randomB,((2**J)*T),num)
        if case2 == num-1:
            return True
    return False

#----------Main-test------------------------
def main():
    rsa = RSA()
    rsa.GenerateKeys(BIGKEY1,BIGKEY2)
    rsa.Encrypt("secret.txt","encrypted.txt")
    rsa.Decrypt("encrypted.txt","decrypted.txt")
#    testchar = "He" 
#    print testchar
#    charnum = stringToValue(testchar,ALPHABET70) 
#    print charnum
#    print valueToString(charnum,ALPHABET70) 
#    test2 = ""
#    for i in range (108):
#        test2 += "b" 
#    test = "hello my name is bob" 
#    print test2
#    stringnum = stringToValue(test2,ALPHABET70)
#    print stringnum
#    print valueToString(stringnum,ALPHABET70)
#    c = pow(stringnum,100000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000829,774818997983558441851378408558353392167575523372874781920908088204814112829748897829538153039739003505476640802179627941864855875656276495661016230675205620411584934524960518705279628953309949044604466040050926265942124752038325330823672003178919005481552444186883849347555121483270020836711112078934479946640405831230902462857547833560729188358360994418793148467473054420612639013849012284957020013)
#    print c
#    m = pow(c,200107726789637981187616079950102776705573322992302471699744092572214098406332538481468733837183132694376611017017987767991770681428018098690615932756202334294336732189897578278235560684473298182938213508155517199700904744503332689752700719530666530556117617927167042765994435952426553895712682782639421024968144282244065796166948188728191223580294229454914509419213392360900467235950926740648857489,774818997983558441851378408558353392167575523372874781920908088204814112829748897829538153039739003505476640802179627941864855875656276495661016230675205620411584934524960518705279628953309949044604466040050926265942124752038325330823672003178919005481552444186883849347555121483270020836711112078934479946640405831230902462857547833560729188358360994418793148467473054420612639013849012284957020013)
#    print m
#    
#    print valueToString(m,ALPHABET70)



main()
